use actix_web::dev::Payload;
use actix_web::http::header::ContentType;
use actix_web::http::header::HeaderValue;
use actix_web::http::StatusCode;
use actix_web::{
    Error, FromRequest, HttpRequest, HttpResponse, HttpResponseBuilder, ResponseError,
};
use jwt::{JWTClaims, JWTResult, UserClaims};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::future::{ready, Ready};

#[derive(Debug)]
pub enum AuthDataError {
    InvalidToken,
}

#[derive(Serialize)]
struct ErrorResponse {
    success: bool,
    message: String,
}

impl Display for AuthDataError {
    fn fmt(&self, _f: &mut Formatter<'_>) -> std::fmt::Result {
        Ok(())
    }
}

impl ResponseError for AuthDataError {
    fn status_code(&self) -> StatusCode {
        match *self {
            AuthDataError::InvalidToken => StatusCode::UNAUTHORIZED,
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponseBuilder::new(self.status_code())
            .insert_header(ContentType::json())
            .json(ErrorResponse {
                success: false,
                message: StatusCode::UNAUTHORIZED.to_string(),
            })
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthData {
    pub user_id: String,
    pub user_slug: Option<String>,
}

impl FromRequest for AuthData {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        let Some(token): Option<&HeaderValue> = req.headers().get("authorization") else {
            return ready(Err(Error::from(AuthDataError::InvalidToken)));
        };

        let mut token: String = token.to_str().unwrap().to_string();

        if !token.contains("Bearer") {
            return ready(Err(Error::from(AuthDataError::InvalidToken)));
        }

        token = token.replace("Bearer ", "");

        let Ok(token_data): JWTResult<JWTClaims<UserClaims>> = jwt::verify(&token) else {
            return ready(Err(Error::from(AuthDataError::InvalidToken)));
        };

        ready(Ok(AuthData {
            user_id: token_data.custom.id.unwrap(),
            user_slug: token_data.custom.slug,
        }))
    }
}
